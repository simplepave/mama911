/**
 * SimplePAVE
 * info@simplepave.ru
 */

jQuery(document).ready(function($){

	/**
	 * Login
	 */

	$('#login-form').submit(function(e) {
		e.preventDefault();
		var t = $(this);

		if(t.hasClass('working')) return;
		t.addClass('working');

      $.ajax({
			url: t.attr('action'),
			type: 'post',
			data: t.serialize(),
			dataType: 'json',
			success: function(json) {
				t.removeClass('working');
				t.find('.text-danger').remove();

				if (json.auth_admin)
					window.location = json.auth_admin;

				if (json.auth_back)
					window.location.reload(true);

				if (json.auth_error)
					t.find('input[type=\'submit\']')
						.before('<div class="text-danger">' + json.auth_error + '</div>');
			},
			error: function(xhr, ajaxOptions, thrownError) {
				console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	});

	/**
	 * Confirm Email Popup
	 */

	if ($('#confirmEmail').length) {
		$.magnificPopup.open({
			items: {src: '#confirmEmail'},
			type: 'inline'
		}, 0);
	}

	/**
	 * Register
	 */

// if (!!window.FileReader) alert('supported'); else alert('not supported');
// if (typeof(window.FileReader) == 'undefined') alert('Browser does not support HTML5 file uploads!'); else alert('HTML5 upload!');
// if (window.FormData !== undefined) alert('да'); else alert('нет');

	$('#register-form').submit(function(e) {
		e.preventDefault();
		var t = $(this);

		if(t.hasClass('working')) return;
		t.addClass('working');

      $.ajax({
			url: t.attr('action'),
			type: 'post',
			data: t.serialize(),
			dataType: 'json',
			success: function(json) {
				t.removeClass('working');
				t.find('.text-success, .text-danger').remove();

				if (json.auth_success) {
					t.before('<div class="text-success">' + json.auth_success + '</div>');
					t.remove();

					setTimeout(function(){
						// window.location.reload(true);
						$.magnificPopup.close();
						// $('.popup[href=\'#entrance\']').click();
					}, 5000);
				}

				if (json.auth_error)
					t.find('input[type=\'submit\']')
						.before('<div class="text-danger">' + json.auth_error + '</div>');
			},
			error: function(xhr, ajaxOptions, thrownError) {
				if (xhr.status === 422) {
					t.removeClass('working');
					t.find('.text-danger').remove();
					var errors = xhr.responseJSON.errors;

					for (var prop in errors) {
						var tegObj  = t.find('[name=\'' + prop + '\']')
						var tegName = tegObj.get(0).tagName.toLowerCase();

						if (tegName =='select')
							t.find('.popup_select').before('<div class="text-danger">' + errors[prop][0] + '</div>');
						else
							tegObj.before('<div class="text-danger">' + errors[prop][0] + '</div>');
					}
				}
				else console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	});

	$('#register-form input').click(function() {
		var t = $(this);

		if (t.prev('.text-danger').length)
			t.prev('.text-danger').remove();
	});

	/**
	 * Region
	 */

	$('#region_form a, #geoRegion-form a').click(function(e) {
		e.preventDefault();
	   var form = $(this).closest('form');

		if(form.hasClass('working')) return;
		form.addClass('working');

		form.find('[name="region"]').val($(this).attr('data-region'));

      $.ajax({
			url: form.attr('action'),
			type: 'post',
			data: form.serialize(),
			dataType: 'json',
			success: function(json) {
				form.removeClass('working');

				if (json.success)
					window.location.reload(true);
			},
			error: function(xhr, ajaxOptions, thrownError) {
				console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	});

	/**
	 * Review
	 */

	$('#review-form').submit(function(e) {
		e.preventDefault();
		var t = $(this);

		if(t.hasClass('working')) return;
		t.addClass('working');

      $.ajax({
			url: t.attr('action'),
			type: 'post',
			data: t.serialize(),
			dataType: 'json',
			beforeSend: function() {
				t.css({'filter': 'grayscale(100%) contrast(90%)'});
			},
			complete: function() {
				t.css({'filter': 'none'});
			},
			success: function(json) {
				t.removeClass('working');
				t.find('.text-success, .text-danger').remove();

				if (json.success) {
					t.before('<div class="text-success">' + json.success + '</div>');
					t.remove();

					var reviews = $('.reviews ul');
					var review = $('<li><div><h3></h3><span></span></div><p></p></li>');
					review.find('h3').text(json.author);
					review.find('span').text(json.created_at);
					review.find('p').text(json.text);
					reviews.prepend(review);
					reviews.find('li.empty').remove();

					$('body,html').animate({
						scrollTop: reviews.parent().offset().top
					}, 800);
				}

				if (json.error)
					t.find('input[type=\'submit\']')
						.before('<div class="text-danger">' + json.error + '</div>');
			},
			error: function(xhr, ajaxOptions, thrownError) {
				if (xhr.status === 422) {
					t.removeClass('working');
					t.find('.text-danger').remove();
					var errors = xhr.responseJSON.errors;

					for (var prop in errors) {
						t.find('[name=\'' + prop + '\']')
							.before('<div class="text-danger">' + errors[prop][0] + '</div>');
					}
				}
				else console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	});

	$('#review-form input, #review-form textarea').click(function() {
		var t = $(this);

		if (t.prev('.text-danger').length)
			t.prev('.text-danger').remove();
	});

	/**
	 * Review More
	 */

	$('#review-more').click(function(e){
		e.preventDefault();
		var t = $(this);

		$.ajax({
			url: t.attr('href'),
			type: 'post',
			data: {_token: t.attr('data-token'), customer: t.attr('data-customer')},
			dataType: 'json',
			beforeSend: function() {
				t.css({'filter': 'grayscale(100%) contrast(90%)'});
			},
			complete: function() {
				t.css({'filter': 'none'});
			},
			success: function(json) {
				if (json.success) {
					if (!json.more) t.remove();
					else t.attr('href', json.next.replace(/&amp;/g, "&"));

					var reviews = $('.reviews ul');
					var review = reviews.find('li').first();

					$.each(json.reviews.data, function(index, value) {
						review = review.clone();
						review.find('h3').text(value.author);
						review.find('span').text(value.data);
						review.find('p').text(value.text);
						reviews.append(review);
					});
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	});

	/**
	 * Search
	 */

	$('#search-form').submit(function(e) {
		e.preventDefault();
		var t = $(this);
		var search = t.find('input[name=\'search\']').val();

		window.location = t.attr('action') + '?q=' + encodeURIComponent(search);
	});

	/**
	 * Profile Form
	 */

	$('#profile-form').submit(function(e) {
		e.preventDefault();
		var t = $(this);

		if(t.hasClass('working')) return;
		t.addClass('working');

      $.ajax({
			url: t.attr('action'),
			type: 'post',
			data: t.serialize(),
			dataType: 'json',
			beforeSend: function() {
				t.find('input[type=\'submit\']').css({'filter': 'grayscale(100%) contrast(90%)'});
			},
			complete: function() {
				t.find('input[type=\'submit\']').css({'filter': 'none'});
			},
			success: function(json) {
				t.removeClass('working');
				t.find('.text-success, .text-danger').remove();

				if (json.success) {
					t.find('input[type=\'submit\']')
						.before('<div class="text-success">' + json.success + '</div>');

					setTimeout(function(){
						window.location.reload(true);
					}, 2000);
				}

				if (json.error)
					t.find('input[type=\'submit\']')
						.before('<div class="text-danger">' + json.error + '</div>');
			},
			error: function(xhr, ajaxOptions, thrownError) {
				if (xhr.status === 422) {
					t.removeClass('working');
					t.find('.text-danger').remove();
					var errors = xhr.responseJSON.errors;

					for (var prop in errors) {
						var tegObj  = t.find('[name=\'' + prop + '\']')
						var tegName = tegObj.get(0).tagName.toLowerCase();

						if (tegName =='select')
							tegObj.closest('.popup_select').before('<div class="text-danger">' + errors[prop][0] + '</div>');
						else
							tegObj.before('<div class="text-danger">' + errors[prop][0] + '</div>');
					}

					$('body,html').animate({
						scrollTop: $('.row_flex').offset().top
					}, 1800);
				}
				else console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	});

	$('#profile-form input').click(function() {
		var t = $(this);

		if (t.prev('.text-danger').length)
			t.prev('.text-danger').remove();
	});

	/**
	 * Profile ('.private_right_col')
	 */

	$('#tabs li .drop_down').click(function(e){
		e.preventDefault();
		$(this).closest('li').addClass('active');
		$(this).remove();
	});

	$(".select_foto > a").click(function(e){
		e.preventDefault();
		$(this).parent().addClass('dropped');
		$(this).remove();
	});

	/**
	 *	Profile Specialty
	 */

	 $("#profile-form .add-spec").click(function(e){
	 	var select = $('#profile-form .popup_select[data-spec="hide"]');
		var t = $(this), li = t.closest('li'), cont = select.length - 1;
		select.first().attr('data-spec', 'show').show();

		if (!li.hasClass('active'))
			li.addClass('active');

		if (!cont)
			t.remove();
	});

	/**
	 * Dropzone Doc Cust
	 */

	$('#deleteDocCurPost, #deleteCustCurPost').click(function(e){
		e.preventDefault();
		var t = $(this);

		$.ajax({
			url: t.attr('href'),
			type: 'post',
			data: {_token: $('#profile-form [name="_token"]').val()},
			dataType: 'json',
			success: function(json) {
				t.closest('li').remove();
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	});

	/**
	 * Phone
	 */

	$('#register-form [name="phone"], #profile-form [name="phone"]').inputmask({
		mask: '+7(999)999-99-99',
		clearMaskOnLostFocus: true,
		clearIncomplete: true
	});

});

/**
 * КЛАДР в облаке
 */

$(function () {
	var $region = $('#register-form [name="region"]');

	$.kladr.setDefault({
		verify: true,
		select: function (obj) {
			$('#register-form [name="region_kladr"]').val(obj.id);
			$('#register-form [name="region_okato"]').val(obj.okato);
			$('#register-form [name="region_type"]').val(obj.type);
			$('#register-form [name="region_type_short"]').val(obj.typeShort);
			// $(this).val(obj.typeShort + '. ' + obj.name);
		},
		check: function (obj) {
			if (!obj)
				$(this).val('')
					.before('<div class="text-danger">Город не найден!</div>');
			else {
				$('#register-form [name="region_kladr"]').val(obj.id);
				$('#register-form [name="region_okato"]').val(obj.okato);
				$('#register-form [name="region_type"]').val(obj.type);
				$('#register-form [name="region_type_short"]').val(obj.typeShort);
			}
		},
		checkBefore: function () {
			if (!$.trim($(this).val()))
				return false;
		}
	});

	$region.kladr({
		type: $.kladr.type.city,
		typeCode: $.kladr.typeCode.city
	});
});

$(function () {
	var $region = $('#profile-form [name="region"]');

	$.kladr.setDefault({
		verify: true,
		select: function (obj) {
			$('#profile-form [name="region_kladr"]').val(obj.id);
			$('#profile-form [name="region_okato"]').val(obj.okato);
			$('#profile-form [name="region_type"]').val(obj.type);
			$('#profile-form [name="region_type_short"]').val(obj.typeShort);
			// $(this).val(obj.typeShort + '. ' + obj.name);
		},
		check: function (obj) {
			if (!obj)
				$(this).val('')
					.before('<div class="text-danger">Город не найден!</div>');
			else {
				$('#register-form [name="region_kladr"]').val(obj.id);
				$('#register-form [name="region_okato"]').val(obj.okato);
				$('#register-form [name="region_type"]').val(obj.type);
				$('#register-form [name="region_type_short"]').val(obj.typeShort);
			}
		},
		checkBefore: function () {
			if (!$.trim($(this).val()))
				return false;
		}
	});

	$region.kladr({
		type: $.kladr.type.city,
		typeCode: $.kladr.typeCode.city
	});
});

/**
 * Cropper
 */

var options = {
    aspectRatio: 1 / 1,
    autoCropArea: 0.8,
    cropBoxResizable: true,
    minCropBoxWidth: 200,
    minCropBoxHeight: 200,
    zoomable: false,
    rotatable: true,
    movable: true,
    dragMode: 'move'
};

if ($('#cropper-image').length)
var cropper = new Cropper(document.getElementById('cropper-image'));

function dataURItoBlob(dataURI) {
    var byteString = atob(dataURI.split(',')[1]);
    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ab], {type: 'image/jpeg'});
}

function cropperDropzone(t, file) {
    var modal = $('#modal-cropper');
    var cachedFilename = file.name;
    var cachedFiletype = file.type;
    t.removeFile(file);

    var fileReader = new FileReader();
    fileReader.readAsDataURL(file);
    fileReader.onloadend = function() {
        var content = fileReader.result;
        $('#cropper-image').attr('src', content);
        cropper.destroy();
        cropper = new Cropper(document.getElementById('cropper-image'), options);
        modal.show();
    }

    modal.on('click', '.crop-upload', function () {
        var blob = cropper.getCroppedCanvas({width: 217, height: 217}).toDataURL(cachedFiletype);
        var croppedFile = dataURItoBlob(blob);
        croppedFile.cropped = true;
        croppedFile.name = cachedFilename;
        t.addFile(croppedFile);
        t.processQueue();
        modal.hide();
    })
    .on('click', '.cropper-close', function () {
        modal.hide();
    });
}

/**
 * Profile
 * Dropzone Photo Customer
 */

var minImageWidth = 800,
    minImageHeight = 800;

Dropzone.options.photoCustomer = {
	url: 'upload.php',
	thumbnailWidth: 213,
	thumbnailHeight: 213,
	maxFiles: 1,
	maxFilesize: 16,
	addRemoveLinks: true,
	dictRemoveFile: 'Удалить',
	dictFileTooBig: 'Изображение больше 16MB',
	timeout: 10000,
	acceptedFiles: 'image/jpeg,image/png,image/gif',

	init: function() {
		$('#photo-customer').val('');

        this.on('thumbnail', function(file) {
            if (file.cropped) return;

            if (file.width < minImageWidth || file.height < minImageHeight) {
                this.removeFile(file);
                alert('Файл меньше чем ' + minImageWidth + 'x' + minImageHeight + '!');
                // console.log("Invalid dimensions!");
            }
            else cropperDropzone(this, file)
        });

		this.on('addedfile', function(file) {
			if (this.files.length > 1)
				this.removeFile(this.files[0]);
		});

		this.on('removedfile', function (file) {
			$('#photo-customer').val('');
		});
	},

	accept: function(file) {
        if (!file.cropped) return;
		var fileReader = new FileReader();

		fileReader.readAsDataURL(file);
		fileReader.onloadend = function() {
			var content = fileReader.result;
			$('#photo-customer').val(content);
			file.previewElement.classList.add("dz-success");
		}
		file.previewElement.classList.add("dz-complete");
    }
}

/**
 * Register
 * Dropzone Register Photo
 */

Dropzone.options.photoUserRegister = {
	url: 'upload.php',
	thumbnailWidth: 217,
	thumbnailHeight: 217,
	maxFiles: 1,
	maxFilesize: 8,
	addRemoveLinks: true,
	dictRemoveFile: 'Удалить',
	dictFileTooBig: 'Изображение больше 8MB',
	timeout: 10000,
	acceptedFiles: 'image/jpeg,image/png,image/gif',

	init: function() {
		$('#photo-user-register').val('');

        this.on('thumbnail', function(file) {
            if (file.cropped) return;
            cropperDropzone(this, file)
        });

		this.on('addedfile', function(file) {
			if (this.files.length > 1)
				this.removeFile(this.files[0]);
		});

		this.on('removedfile', function (file) {
			$('#photo-user-register').val('');
		});
	},

	accept: function(file) {
        if (!file.cropped) return;
		var fileReader = new FileReader();

		fileReader.readAsDataURL(file);
		fileReader.onloadend = function() {
			var content = fileReader.result;
			$('#photo-user-register').val(content);
			file.previewElement.classList.add("dz-success");
		}
		file.previewElement.classList.add("dz-complete");
    }
}

/**
 * Dropzone Register Photo Document
 */

Dropzone.options.photoUserDoc = {
	url: 'upload.php',
	thumbnailWidth: 217,
	thumbnailHeight: 217,
	maxFiles: 1,
	maxFilesize: 8,
	addRemoveLinks: true,
	dictRemoveFile: 'Удалить',
	dictFileTooBig: 'Изображение больше 8MB',
	timeout: 10000,
	acceptedFiles: 'image/jpeg,image/png,image/gif',

	init: function() {
		$('#photo-user-doc').val('');

		this.on('addedfile', function(file) {
			if (this.files.length > 1)
				this.removeFile(this.files[0]);
		});

		this.on('removedfile', function (file) {
			$('#photo-user-doc').val('');
		});
	},

	accept: function(file) {
		var fileReader = new FileReader();

		fileReader.readAsDataURL(file);
		fileReader.onloadend = function() {
			var content = fileReader.result;
			$('#photo-user-doc').val(content);
			file.previewElement.classList.add("dz-success");
		}
		file.previewElement.classList.add("dz-complete");
    }
}

/**
 * Dropzone Register Photo Customer
 */

Dropzone.options.photoUserCustomer = {
	url: 'upload.php',
	thumbnailWidth: 217,
	thumbnailHeight: 217,
	maxFiles: 1,
	maxFilesize: 8,
	addRemoveLinks: true,
	dictRemoveFile: 'Удалить',
	dictFileTooBig: 'Изображение больше 8MB',
	timeout: 10000,
	acceptedFiles: 'image/jpeg,image/png,image/gif',

	init: function() {
		$('#photo-user-customer').val('');

		this.on('addedfile', function(file) {
			if (this.files.length > 1)
				this.removeFile(this.files[0]);
		});

		this.on('removedfile', function (file) {
			$('#photo-user-customer').val('');
		});
	},

	accept: function(file) {
		var fileReader = new FileReader();

		fileReader.readAsDataURL(file);
		fileReader.onloadend = function() {
			var content = fileReader.result;
			$('#photo-user-customer').val(content);
			file.previewElement.classList.add("dz-success");
		}
		file.previewElement.classList.add("dz-complete");
    }
}