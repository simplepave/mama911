$(document).ready(function(){

	$('#phone-user-card-show a').click(function(e){
		e.preventDefault();
		$(this).parent('#phone-user-card-show').hide();
		$('#phone-user-card').show();
	});

	$(".popup").magnificPopup({type:"inline"});

	$("#toTop").hide();
		$(function () {
			$(window).scroll(function () {
				if ($(this).scrollTop() > 50) {
					$('#toTop').fadeIn();
				} else {
					$('#toTop').fadeOut();
				}
			});
			$('#toTop').click(function () {
				$('body,html').animate({
					scrollTop: 0
				}, 800);
				return false;
			});
		});

	//$('select').styler({ selectSearch: true });


	// if($('#tabs').length){

	// 	$('#tabs li .drop_down').click(function(){
	// 		if($(this).closest('li').hasClass('active')) {
	// 			$(this).closest('li').removeClass('active');
	// 		} else {
	// 			$('#tabs li').removeClass('active');
	// 			$(this).closest('li').addClass('active');
	// 		}
	// 		return false;
	// 	});
	// }

	// if($(".select_foto > a").length) {
	// 	$(".select_foto > a").click(function(){
	// 		$(this).parent().toggleClass('dropped');
	// 		return false;
	// 	});
	// }

	// $('.edit').on('click', function(){
	// 	$(this).parent().addClass('dropped');
	// 	return false;
	// });

	$('#rating').rating({
		fx: 'full',
		click: function(responce) {
			$('#review-form [name="rating"]').val(responce);
      }
	});

});

$(document).ready(function() {
	var owl = $('.registered_slider');
    	owl.owlCarousel({
		margin:0,
		nav: true,
		loop: true,
		smartSpeed:500,
		responsive: {
			0: {
			items: 1
			},
			480: {
			items: 2
			},
			600: {
			items: 3
			},
			700: {
			items: 4
			},
			1050: {
			items: 5
			}
		}
	})
})


$(function(){
	$('input[placeholder], textarea[placeholder]').placeholder();
});