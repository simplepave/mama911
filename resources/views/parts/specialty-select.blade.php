@php ($menu_item_id = isset($menu_item_id)? $menu_item_id: '')
<option disabled>- Малышам -</option>
{{ menu('babies', 'menu.specialty-select', ['menu_item_id' => $menu_item_id]) }}
<option disabled>- Будущим мамам -</option>
{{ menu('future_mothers', 'menu.specialty-select', ['menu_item_id' => $menu_item_id]) }}
<option disabled>- Мамам -</option>
{{ menu('moms', 'menu.specialty-select', ['menu_item_id' => $menu_item_id]) }}