@component('mail::message')

<h1>Спасибо за регистрацию, {{ $user_name }}!</h1>

@component('mail::button', ['url' => $url])
Подтвердить E-mail
@endcomponent

Добро пожаловать на наш сайт!
<p><a href="{{ url('/') }}" target="_blank">{{ config('app.name') }}</a></p>
@endcomponent