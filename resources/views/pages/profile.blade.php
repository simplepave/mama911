@extends('layouts.base')

@section('title'){{ $title or '' }}@endsection
@section('keywords'){{ $keywords or '' }}@endsection

@section('container')
<section class="private_office">
    <h1>Личная информация</h1>
    <div class="row_flex">
        <form id="profile-form" action="{{ route('site.profilePost') }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="private_left_col">
            <div class="select_foto">
                <input type="hidden" id="photo-customer" name="photo_customer" value="">
                <div class="dropzone dropzone-file-area" id="photoCustomer">
                    <div class="dz-default dz-message img">
                        <div>
                            <a href="#" onclick="return false;">Изменить фото</a>
                            <img src="{{ url('storage/' . $user->avatar) }}" alt="">
                        </div>
                    </div>
                </div>
                <div class="like">
                    @php ($aggregate = isset($customer->avgRating[0]->aggregate)? $customer->avgRating[0]->aggregate: 0)
                    @if ($aggregate)
                    <span>{{ number_format($aggregate, 1, ',', '') }}</span>
                    <ul>
                        @for ($i = 1; $i <= 5; $i++)
                        @if (round($aggregate) < $i)
                        <li style="background-position-y:bottom;"></li>
                        @else
                        <li></li>
                        @endif
                        @endfor
                    </ul>
                    @else
                    <span>Пока нет рейтинга</span>
                    @endif
                </div>
                <div class="phone-number-profile"><strong>{{ $customer->phone }}</strong></div>
                <input autocomplete="off" name="phone" class="input_dropp" type="text" value="{{ $customer->phone }}" placeholder="Номер телефона">
                <a href="#">Редактировать номер</a>
            </div>
        </div>
        <div class="private_right_col">
            <div class="des">
                <ul id="tabs">
                    <li>
                        <strong>ФИО</strong>
                        <div><p>{{ $user->name }}</p>
                        <input autocomplete="off" name="name" class="input_dropp" type="text" value="{{ $user->name }}">
                        <a class="drop_down" href="#"></a></div>
                    </li>
                    <li>
                        <strong>Специальность</strong>
                        <div><p>{{ $customer->specialties[0]->titles }}</p>
                        @foreach ($specialties as $item)
                            @if (!$loop->first and $item->item > $loop->iteration)
                            @for ($i = $loop->iteration; $i < $item->item; $i++)
                            <div data-spec="hide" class="popup_select" style="display: none;">
                                <select autocomplete="off" name="specialty_{{ $i }}">
                                    <option value=""> --- Специальность --- </option>
                                    {!! $specialty !!}
                                </select>
                            </div>
                            @endfor
                            @endif
                            <div class="popup_select">
                                <select autocomplete="off" name="specialty_{{ $item->item }}">
                                    <option value=""> --- Специальность --- </option>
                                    @include('parts.specialty-select', ['menu_item_id' => $item->specialty])
                                </select>
                            </div>
                            @if ($loop->last and $item->item < 5)
                            @for ($i = $item->item + 1; $i <= 5; $i++)
                            <div data-spec="hide" class="popup_select" style="display: none;">
                                <select autocomplete="off" name="specialty_{{ $i }}">
                                    <option value=""> --- Специальность --- </option>
                                    {!! $specialty !!}
                                </select>
                            </div>
                            @endfor
                            <div class="add-spec">Добавить специальность</div>
                            @endif
                        @endforeach
                        <a class="drop_down" href="#"></a></div>
                    </li>
                    <li>
                        <strong>Образование</strong>
                        <div><p>{{ $customer->education }}</p>
                        <input autocomplete="off" name="education" class="input_dropp" type="text" value="{{ $customer->education }}">
                        <a class="drop_down" href="#"></a></div>
                    </li>
                    <li>
                        <strong>Стаж</strong>
                        <div><p>{{ $customer->experience }}</p>
                        <input autocomplete="off" name="experience" class="input_dropp" type="text" value="{{ $customer->experience }}">
                        <a class="drop_down" href="#"></a></div>
                    </li>
                    <li>
                        <strong>Город</strong>
                        <div><p>{{ $customer->region->type_short }}. {{ $customer->region->name }}</p>
                        <input autocomplete="off" type="hidden" name="region_kladr" value="{{ $customer->region->kladr }}">
                        <input autocomplete="off" type="hidden" name="region_okato" value="{{ $customer->region->okato }}">
                        <input autocomplete="off" type="hidden" name="region_type" value="{{ $customer->region->type }}">
                        <input autocomplete="off" type="hidden" name="region_type_short" value="{{ $customer->region->type_short }}">
                        <input autocomplete="off" name="region" class="input_dropp" type="text" value="{{ $customer->region->name }}">
                        <a class="drop_down" href="#"></a></div>
                    </li>
                    <li>
                        <strong>Адрес приёма</strong>
                        <div><p>{{ $customer->address }}</p>
                        <input autocomplete="off" name="address" class="input_dropp" type="text" value="{{ $customer->address }}">
                        <a class="drop_down" href="#"></a></div>
                    </li>
                    <li>
                        <strong>Цена от</strong>
                        <div><p>{{ number_format($customer->price, 0, '', ' ') }} руб.</p>
                        <input autocomplete="off" name="price" class="input_dropp" type="text" value="{{ $customer->price }}">
                        <a class="drop_down" href="#"></a></div>
                    </li>
                    <li>
                        <strong>Цена до</strong>
                        <div><p>{{ number_format($customer->price_hour, 0, '', ' ') }} руб.</p>
                        <input autocomplete="off" name="price_hour" class="input_dropp" type="text" value="{{ $customer->price_hour }}">
                        <a class="drop_down" href="#"></a></div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="text-about-me">
            <label for="aboutMeText">Обо мне </label>
            <textarea name="about" id="aboutMeText" placeholder="Напишите о себе..." rows="10">{{ $customer->about }}</textarea>
        </div>
        <div class="row_my">
        <div class="photos_documents">
                <h3>Фото документов</h3>
                @if ($photo_doc and $photo_doc->isNotEmpty())
                <ul id="ul-photo-documents" class="owl-carousel">
                    @foreach ($photo_doc as $item)
                    <li>
                        <a href="{{ url('storage/' . $item->original_name) }}" data-lightbox="photodocs">
                            <img src="{{ url('storage/' . $item->resized_name) }}" alt="">
                        </a><a id="deleteDocCurPost" class="edit-delete-image" href="{{ route('site.upload.deleteDocCurPost', ['id' => $item->id]) }}">Удалить</a>
                    </li>
                    @endforeach
                </ul>
                @endif
                <div class="dropzone dropzone-file-area" id="myPhotosDocuments">
                    <div class="dz-default dz-message">Нажмите или перенесите сюда изображения для добавления.</div>
                </div>
        </div>
        <div class="photos_customers">
                <h3>Фото с клиентами</h3>
                @if ($photo_customer and $photo_customer->isNotEmpty())
                <ul id="userPhotoCustomers" class="owl-carousel">
                    @foreach ($photo_customer as $item)
                    <li>
                        <a href="{{ url('storage/' . $item->original_name) }}" data-lightbox="photocustomers">
                            <img src="{{ url('storage/' . $item->resized_name) }}" alt="">
                        </a><a id="deleteCustCurPost" class="edit-delete-image" href="{{ route('site.upload.deleteCustCurPost', ['id' => $item->id]) }}">Удалить</a>
                    </li>
                    @endforeach
                </ul>
                @endif
                <div class="dropzone dropzone-file-area" id="myPhotosCustomers">
                    <div class="dz-default dz-message">Нажмите или перенесите сюда изображения для добавления.</div>
                </div>
            </div>
        </div>
        <div class="profile-foot">
            <input class="submit" type="submit" value="Сохранить изменения">
            </form>
        </div>
    </div>
</section>
<script type="text/javascript">
jQuery(document).ready(function($){

/**
 * Owl Carousel
 */

    $(".owl-carousel").owlCarousel({
        nav: true
    });

/**
 * Photo Documents
 */

var fileListDoc = [];
Dropzone.options.myPhotosDocuments = {
    url: '{{ route('site.upload.saveDocPost') }}',
    // autoProcessQueue: true,
    maxFiles: 15 - {{ $photo_doc? count($photo_doc): 0 }},
    uploadMultiple: true,
    parallelUploads: 1,
    acceptedFiles: 'image/jpeg,image/png,image/gif',
    maxFilesize: 8,
    thumbnailWidth: null,
    thumbnailHeight: 127,
    addRemoveLinks: true,
    dictRemoveFile: 'Удалить',
    dictFileTooBig: 'Изображение больше 8MB',
    timeout: 10000,
    init: function() {
        this.on("sending", function(file, xhr, formData) {
            formData.append('_token', $('#profile-form [name="_token"]').val());
        });

        this.on("removedfile", function (file) {
            $.ajax({
                url: '{{ route('site.upload.deleteDocPost') }}',
                type: 'post',
                data: {data: JSON.stringify(fileListDoc), _token: $('#profile-form [name="_token"]').val(), uuid: file.upload.uuid},
                dataType: 'html',
                success: function (data) {
                    // console.log(data);
                }
            });
        });

        this.on("success", function(file, server) {
            fileListDoc.push({"uploadId" : server.id, "uuid" : file.upload.uuid });
        });

        this.on("maxfilesexceeded", function(file) {
            this.removeFile(file);
        });
    }
};

/**
 * Photo Customers
 */

var fileListCust = [];
Dropzone.options.myPhotosCustomers = {
    url: '{{ route('site.upload.saveCustPost') }}',
    // autoProcessQueue: true,
    maxFiles: 15 - {{ $photo_customer? count($photo_customer): 0 }},
    uploadMultiple: true,
    parallelUploads: 1,
    acceptedFiles: 'image/jpeg,image/png,image/gif',
    maxFilesize: 8,
    thumbnailWidth: null,
    thumbnailHeight: 127,
    addRemoveLinks: true,
    dictRemoveFile: 'Удалить',
    dictFileTooBig: 'Изображение больше 8MB',
    timeout: 10000,
    init: function() {
        this.on("sending", function(file, xhr, formData) {
            formData.append('_token', $('#profile-form [name="_token"]').val());
        });

        this.on("removedfile", function (file) {
            $.ajax({
                url: '{{ route('site.upload.deleteCustPost') }}',
                type: 'post',
                data: {data: JSON.stringify(fileListCust), _token: $('#profile-form [name="_token"]').val(), uuid: file.upload.uuid},
                dataType: 'html',
                success: function (data) {
                    // console.log(data);
                }
            });
        });

        this.on("success", function(file, server) {
            fileListCust.push({"uploadId" : server.id, "uuid" : file.upload.uuid });
        });

        this.on("maxfilesexceeded", function(file) {
            this.removeFile(file);
        });
    }
};
});
</script>
@endsection

@section('cropper')
@include('common.cropper')
@endsection