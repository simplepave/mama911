@extends('layouts.base')

@section('title'){{ $title or '' }}@endsection
@section('keywords'){{ $keywords or '' }}@endsection

@section('container')
<section class="container search-result-cont">
    @include('common.bread')
    <div class="mothers row_flex">
        <div class="right_col">
            <div class="mothers_list">
                @if ($customers)
                <ul>
                    @forelse($customers as $customer)
                    <li>
                        <div class="row_flex">
                            <div class="icon"><img src="{{ url('storage/' . $customer->avatar) }}" alt="{{ $customer->name }}"></div>
                            <div class="text">
                                <a href="{{ route('site.user.card', ['id' => $customer->id]) }}">{{ $customer->name }}</a>
                                <p>{{ $customer->specialty }}</p>
                                <div class="row_flex">
                                    <div class="like">
                                        @php ($aggregate = isset($customer->aggregate)? $customer->aggregate: 0)
                                        @if ($aggregate)
                                        <span>{{ number_format($aggregate, 1, ',', '') }}</span>
                                        <ul>
                                            @for ($i = 1; $i <= 5; $i++)
                                            @if (round($aggregate) < $i)
                                            <li style="background-position-y:bottom;"></li>
                                            @else
                                            <li></li>
                                            @endif
                                            @endfor
                                        </ul>
                                        @else
                                        <span>Пока нет рейтинга</span>
                                        @endif
                                    </div>
                                    <div class="city">{{ $customer->region_type }}. {{ $customer->region_name }}</div>
                                </div>
                            </div>
                            <div class="prise">
                                <div>
                                    <span>{{ number_format($customer->price, 0, '', ' ') }} руб.</span>
                                    <p>цена от</p>
                                </div>
                                <span>-</span>
                                <div>
                                    <strong>{{ number_format($customer->price_hour, 0, '', ' ') }} руб.</strong>
                                    <p>цена до</p>
                                </div>
                            </div>
                        </div>
                    </li>
                    @empty
                    <li>Нет специалистов для отображения</li>
                    @endforelse
                </ul>
                @if ($customers->lastPage() > 1)
                <div class="pagin">
                    {{ $customers->appends(request()->except('page'))->links() }}
                </div>
                @endif
                @else
                <p>Ничего не найдено!</p>
                @endif
            </div>
        </div>
    </div>
</section>
@endsection