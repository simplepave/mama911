@extends('layouts.base')

@section('title'){{ $title or '' }}@endsection
@section('keywords'){{ $keywords or '' }}@endsection

@section('container')
<section class="main_article">
	<h2>{{ $title }}</h2>
    @include('common.bread')
    @if ($posts)
    <ul>
        @forelse($posts as $post)
    	<li>
        	<div class="img">
                <a href="{{ url('/article/' . $post->slug) }}"><img src="{{ url('/storage/' . $post->image) }}" alt="{{ $post->title }}"></a>
            </div>
            <div class="text">
                <a href="{{ url('/article/' . $post->slug) }}">{{ $post->title }}</a>
            </div>
        </li>
        @empty
        <li>Нет статей для отображения</li>
        @endforelse
    </ul>
    @if ($posts->lastPage() > 1)
    <div class="pagin">
        {{ $posts->links() }}
    </div>
    @endif
    @endif
</section>
@endsection