@extends('layouts.base')

@section('title'){{ $title or '' }}@endsection
@section('keywords'){{ $keywords or '' }}@endsection

@section('container')
<section class="article_detail">
	<h1>{{ $title }}</h1>
	@include('common.bread')
    <div class="detail_wrapp">
    	<div class="row_flex">
        	<div class="img"><img src="{{ url('/storage/' . $post->image) }}" alt="{{ $post->title }}"></div>
            <div class="text">
            	<h3>{{ $post->title }}</h3>
                <p>{{ $post->excerpt }}</p>
            </div>
        </div>
        {!! $post->body !!}
    </div>
</section>
@if ($other_articles)
<section class="main_article">
	<h2>Другие статьи</h2>
    <ul>
        @foreach($other_articles as $post)
        <li>
            <div class="img">
                <a href="{{ url('/article/' . $post->slug) }}"><img src="{{ url('/storage/' . $post->image) }}" alt="{{ $post->title }}"></a>
            </div>
            <div class="text">
                <a href="{{ url('/article/' . $post->slug) }}">{{ $post->title }}</a>
            </div>
        </li>
        @endforeach
    </ul>
</section>
@endif
@endsection