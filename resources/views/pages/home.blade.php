@extends('layouts.base')

@section('title'){{ $title or 'Главная' }}@endsection
@section('keywords'){{ $keywords or '' }}@endsection

@section('container')
    <div class="main_title">
        <h1>Лучшие специалисты для мам и малышей</h1>
        <div><img src="{{ url('/images/content/img-main-title.png') }}" alt=""></div>
    </div>
    <ul class="select_category">
        <li>
            <a href="{{ route('site.moms') }}">
                <div><img src="{{ url('/images/select-category/icon1.png') }}" alt=""></div>
                <p>Мамам</p>
            </a>
        </li>
        <li>
            <a href="{{ route('site.babies') }}">
                <div><img src="{{ url('/images/select-category/icon2.png') }}" alt=""></div>
                <p>Малышам</p>
            </a>
        </li>
    </ul>
    @if ($customers->isNotEmpty())
    <section class="recently_registered">
        <h2>Недавно зарегистрированные</h2>
        <div class="registered_slider owl-carousel">
            @foreach ($customers as $customer)
            <div class="item">
                <div>
                    <a href="{{ route('site.user.card', ['id' => $customer->id]) }}">
                        <img src="{{ url('storage/' . $customer->avatar) }}" alt="">
                    </a>
                </div>
                <p>{{ $customer->title }}</p>
            </div>
            @endforeach
        </div>
    </section>
    @endif
    @if ($articles_home)
    <section class="main_article">
        <h2>Статьи</h2>
        <ul>
            @foreach($articles_home as $post)
            <li>
                <div class="img">
                    <a href="{{ url('/article/' . $post->slug) }}"><img src="{{ url('/storage/' . $post->image) }}" alt="{{ $post->title }}"></a>
                </div>
                <div class="text">
                    <a href="{{ url('/article/' . $post->slug) }}">{{ $post->title }}</a>
                </div>
            </li>
            @endforeach
        </ul>
        <a href="{{ route('site.articles') }}">Все статьи</a>
    </section>
    @endif
    <section class="section_specialists">
        <h2>Разделы специалистов</h2>
        <div class="row_flex">
            <div class="col">
                <div class="icon"><img src="{{ url('/images/section-specialists/icon1.jpg') }}" alt=""></div>
                <div class="head">
                    <strong>Малышам</strong>
                    <div>{{ menu_count_customer('babies') }}</div>
                </div>
                {{ menu('babies', 'menu.sections-menu') }}
            </div>
            <div class="col">
                <div class="icon"><img src="{{ url('/images/section-specialists/icon2.jpg') }}" alt=""></div>
                <div class="head">
                    <strong>Будущим мамам</strong>
                    <div>{{ menu_count_customer('future_mothers') }}</div>
                </div>
                {{ menu('future_mothers', 'menu.sections-menu') }}
            </div>
            <div class="col">
                <div class="icon"><img src="{{ url('/images/section-specialists/icon3.jpg') }}" alt=""></div>
                <div class="head">
                    <strong>Мамам</strong>
                    <div>{{ menu_count_customer('moms') }}</div>
                </div>
                {{ menu('moms', 'menu.sections-menu') }}
            </div>
        </div>
    </section>
@endsection