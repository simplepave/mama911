@extends('layouts.base')

@section('title'){{ $title or '' }}@endsection
@section('keywords'){{ $keywords or '' }}@endsection

@section('container')
<section class="private_office user-card-body">
    <h1>{{ $user->name }}</h1>
    <div class="row_flex">
        <div class="private_left_col">
            <div class="select_foto">
                <div class="img"><div><img src="{{ url('storage/' . $user->avatar) }}" alt="{{ $user->name }}"></div></div>
                <div class="like">
                    @php ($aggregate = isset($customer->avgRating[0]->aggregate)? $customer->avgRating[0]->aggregate: 0)
                    @if ($aggregate)
                    <span>{{ number_format($aggregate, 1, ',', '') }}</span>
                    <ul>
                        @for ($i = 1; $i <= 5; $i++)
                        @if (round($aggregate) < $i)
                        <li style="background-position-y:bottom;"></li>
                        @else
                        <li></li>
                        @endif
                        @endfor
                    </ul>
                    @else
                    <span>Пока нет рейтинга</span>
                    @endif
                </div>
                <div id="phone-user-card" style="display: none" class="phone-number-profile"><strong>{{ $customer->phone }}</strong></div>
                <div id="phone-user-card-show" class="button"><a href="#">Показать номер</a></div>
            </div>
        </div>
        <div class="private_right_col">
            <div class="des">
                <ul>
                    <li>
                        <strong>Специальность</strong>
                        <p>{{ $customer->specialties[0]->titles }}</p>
                    </li>
                    <li>
                        <strong>Образование</strong>
                        <p>{{ $customer->education }}</p>
                    </li>
                    <li>
                        <strong>Стаж</strong>
                        <p>{{ $customer->experience }}</p>
                    </li>
                    <li>
                        <strong>Город</strong>
                        <p>{{ $customer->region->type_short }}. {{ $customer->region->name }}</p>
                    </li>
                    <li>
                        <strong>Адрес приёма</strong>
                        <p>{{ $customer->address }}</p>
                    </li>
                    <li>
                        <strong>Цена от</strong>
                        <p>{{ number_format($customer->price, 0, '', ' ') }} руб.</p>
                    </li>
                    <li>
                        <strong>Цена до</strong>
                        <p>{{ number_format($customer->price_hour, 0, '', ' ') }} руб.</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    @if ($customer->about)
    <div class="output-about-me">
        <div class="title-about-me">Обо мне </div>
        <p>{{ $customer->about }}</p>
    </div>
    @endif
    <div class="row_my">
        @if ($photo_doc and $photo_doc->isNotEmpty())
            <div class="photos_documents">
                <h3>Фото документов</h3>
                <ul class="owl-carousel">
                    @foreach ($photo_doc as $item)
                    <li><a href="{{ url('storage/' . $item->original_name) }}" data-lightbox="photodocs""><img src="{{ url('storage/' . $item->resized_name) }}" alt=""></a></li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if ($photo_customer and $photo_customer->isNotEmpty())
            <div class="photos_customers">
                <h3>Фото с клиентами</h3>
                <ul class="owl-carousel">
                    @foreach ($photo_customer as $item)
                    <li><a href="{{ url('storage/' . $item->original_name) }}" data-lightbox="photocustomers""><img src="{{ url('storage/' . $item->resized_name) }}" alt=""></a></li>
                    @endforeach
                </ul>
            </div>
            @endif
    </div>
    <div class="reviews">
        <h2>Отзывы</h2>
        <ul>
            @forelse($reviews as $review)
            <li>
                <div>
                    <h3>{{ $review->author }}</h3>
                    <span>{{ \Carbon\Carbon::parse($review->created_at)->format('d.m.Y') }}</span>
                </div>
                <p>{{ $review->text }}</p>
            </li>
            @empty
            <li class="empty">Пока нет отзывов</li>
            @endforelse
        </ul>
        @if ($reviews->hasMorePages())
        <a id="review-more" data-token="{{ csrf_token() }}" data-customer="{{ $customer->id }}" href="{{ route('site.review.next') . '?page=' }}{{ $reviews->currentPage() + 1 }}">Показать еще</a>
        @endif
    </div>
    <div class="feedback">
        <h2>Оставить отзыв</h2>
        <form id="review-form" action="{{ route('site.review.addPost') }}" method="post">
            {{ csrf_field() }}
            <div class="row_flex">
                <div class="col"><input name="author" class="input" type="text" placeholder="Ваше имя"></div>
                <div class="col"><input name="email" class="input" type="email" placeholder="Ваша почта"></div>
                <div class="col row_flex">
                    <p>Оценить специалиста</p>
                    <div class="like">
                        <div id="rating"></div>
                    </div>
                </div>
            </div>
            <textarea name="text" placeholder="Ваш отзыв"></textarea>
            <input type="hidden" name="customer_id" value="{{ $customer->id }}">
            <input type="hidden" name="rating" value="0">
            <div class="align"><input class="submit" type="submit" value="Отправить"></div>
        </form>
    </div>
</section>
<script type="text/javascript">
    jQuery(document).ready(function($){
     $(".owl-carousel").owlCarousel({
        nav: true
    });
 });
</script>
@endsection