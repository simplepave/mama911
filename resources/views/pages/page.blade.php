@extends('layouts.base')

@section('title'){{ $title or '' }}@endsection
@section('keywords'){{ $keywords or '' }}@endsection

@section('container')
<section class="article_detail">
	<h1>{{ $title }}</h1>
	@include('common.bread')
    <div class="detail_wrapp">
    	<div class="row_flex">
        	<div class="img"><img src="{{ url('/storage/' . $page->image) }}" alt="{{ $page->title }}"></div>
            <div class="text">
            	<h3>{{ $page->title }}</h3>
                <p>{{ $page->excerpt }}</p>
            </div>
        </div>
        {!! $page->body !!}
    </div>
</section>
@endsection