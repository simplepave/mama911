<div id="entrance" class="entrance">
    <h3>Вход</h3>
    <form id="login-form" method="POST" action="{{ route('site.auth.loginPost') }}">
    	{{ csrf_field() }}
        <input name="email" class="input" type="text" placeholder="E-mail">
        <input name="password" class="input" type="password" placeholder="Пароль">
        <input class="submit" type="submit" value="Войти">
        <p>Нажимая кнопку, вы принимаете <a href="{{ route('site.pages', ['slug' => 'position']) }}">Положение</a> и <a href="{{ route('site.pages', ['slug' => 'agreement']) }}">Согласие</a> на обработку данных.</p>
    </form>
</div>