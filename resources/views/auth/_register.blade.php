<div id="registration" class="registration">
    <h3>Регистрация для специалистов</h3>
    <form id="register-form" method="post" action="{{ route('site.auth.registerPost') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" id="photo-user-register" name="photo_user_register" value="">
        <div class="dropzone dropzone-file-area" id="photoUserRegister">
            <div class="dz-default dz-message foto_popup">
                <span class="cross"></span><p>Фото</p>
            </div>
        </div>
        <input name="name" class="input" type="text" placeholder="ФИО">
        <input name="phone" class="input" type="text" placeholder="Номер телефона">
        <input name="email" class="input" type="text" placeholder="E-mail">
        <input name="password" class="input" type="password" placeholder="Пароль">
        <input name="password2" class="input" type="password" placeholder="Повторить пароль">
        <div class="popup_select">
            <select name="specialty">
                <option value=""> --- Специальность --- </option>
                @include('parts.specialty-select')
            </select>
        </div>
        <input name="education" class="input" type="text" placeholder="Образование">
        <input name="experience" class="input" type="text" placeholder="Стаж">
        <input type="hidden" name="region_kladr" value="">
        <input type="hidden" name="region_okato" value="">
        <input type="hidden" name="region_type" value="">
        <input type="hidden" name="region_type_short" value="">
        <input name="region" class="input" type="text" placeholder="Город">
        <input name="address" class="input" type="text" placeholder="Адрес">
        <input name="price" class="input" type="text" placeholder="Цена от">
        <input name="price_hour" class="input" type="text" placeholder="Цена до">
        <input type="hidden" id="photo-user-doc" name="photo_user_doc" value="">
        <div class="dropzone dropzone-file-area" id="photoUserDoc">
            <div class="dz-default dz-message foto">
                <span class="cross"></span><p>Фото документов</p>
            </div>
        </div>
        <input type="hidden" id="photo-user-customer" name="photo_user_customer" value="">
        <div class="dropzone dropzone-file-area" id="photoUserCustomer">
            <div class="dz-default dz-message foto">
                <span class="cross"></span><p>Фото с клиентами</p>
            </div>
        </div>
        <input class="submit" type="submit" value="Зарегистрироваться">
        <p>Нажимая кнопку, вы принимаете <a href="{{ route('site.pages', ['slug' => 'position']) }}">Положение</a> и <a href="{{ route('site.pages', ['slug' => 'agreement']) }}">Согласие</a> на обработку данных.</p>
    </form>
</div>