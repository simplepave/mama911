<div id="registration" class="registration">
    <h3>Регистрация для специалистов</h3>
    <form id="register-form" method="post" action="{{ route('site.auth.registerPost') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input name="name" class="input" type="text" placeholder="ФИО">
        <input name="phone" class="input" type="text" placeholder="Номер телефона">
        <input name="email" class="input" type="text" placeholder="E-mail">
        <input name="password" class="input" type="password" placeholder="Пароль">
        <input name="password2" class="input" type="password" placeholder="Повторить пароль">
        <div class="popup_select">
            <select name="specialty">
                <option value=""> --- Специальность --- </option>
                @include('parts.specialty-select')
            </select>
        </div>
        <input type="hidden" name="region_kladr" value="">
        <input type="hidden" name="region_okato" value="">
        <input type="hidden" name="region_type" value="">
        <input type="hidden" name="region_type_short" value="">
        <input name="region" class="input" type="text" placeholder="Город">
        <input class="submit" type="submit" value="Зарегистрироваться">
        <p>Нажимая кнопку, вы принимаете <a href="{{ route('site.pages', ['slug' => 'position']) }}">Положение</a> и <a href="{{ route('site.pages', ['slug' => 'agreement']) }}">Согласие</a> на обработку данных.</p>
    </form>
</div>