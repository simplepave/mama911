<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="yandex-verification" content="12433ab9309b890d">
    <meta name="keywords" content="@yield('keywords')">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="/css/magnific-popup.css">
    <link rel="stylesheet" href="/css/owl.carousel.css">
    <link rel="stylesheet" href="/css/cropper.min.css">
    <link rel="stylesheet" href="/css/dropzone.min.css">
    <link rel="stylesheet" href="/css/jquery.kladr.min.css">
    <link rel="stylesheet" href="/css/styles.css">
    <link rel="stylesheet" href="/css/media.css">
    <link rel="stylesheet" href="/css/jquery.rating.css">
    <link rel="stylesheet" href="/css/lightbox.min.css">
    @stack('head_styles')
    <script type="text/javascript" src="/js/jquery-2.0.3.min.js"></script>
    @stack('head_scripts')
</head>
<body>
    @include('common.header')
    @include('common.search')
    @yield('container')
    @include('common.footer')
    <a id="toTop" href="#"><i></i></a>
    @guest
        @include('auth.login')
        @include('auth.register')
        @include('common.cropper')
    @endguest
    @yield('cropper')
    @if (session('confirmEmail'))
        @include('auth.confirm-email', ['text' => session('confirmEmail')])
    @endif
    @inject('regions', 'App\Http\Controllers\RegionController')
    {!! $regions->index() !!}
    <script type="text/javascript" src="/js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="/js/jquery.magnific-popup.js"></script>
    <script type="text/javascript" src="/js/jquery.formstyler.js"></script>
    <script type="text/javascript" src="/js/jquery.rating-2.0.min.js"></script>
    <script type="text/javascript" src="/js/jquery.placeholder.min.js"></script>
    <script type="text/javascript" src="/js/cropper.min.js"></script>
    <script type="text/javascript" src="/js/dropzone.min.js"></script>
    <script type="text/javascript" src="/js/jquery.kladr.min.js"></script>
    <script type="text/javascript" src="/js/lightbox.min.js"></script>
    <script type="text/javascript" src="/js/jquery.inputmask.bundle.min.js"></script>
    <script type="text/javascript" src="/js/scripts.js"></script>
    <script type="text/javascript" src="/js/sp.js"></script>
    @stack('bottom_scripts')
</body>
</html>