@php ($item = '')
@foreach($items as $menu_item)
	@php ($is_active = '')
	@if (url($menu_item->link()) == url()->current())
		@php ($is_active = ' class="active"')
	@endif
	@if (empty($menu_item->url))
		<h3>{{ $menu_item->title }}</h3>
		@if (count($menu_item->children))
		<ul>
			@foreach($menu_item->children as $menu_child)
				@php ($is_active_child = '')
				@if (url($menu_child->link()) == url()->current())
					@php ($is_active_child = ' class="active"')
				@endif
				<li{!! $is_active_child !!}><a href="{{ url($menu_child->link()) }}">{{ $menu_child->title }}</a></li>
			@endforeach
		</ul>
		@endif
	@else
		@php ($item .= '<li' . $is_active . '><a href="' . url($menu_item->link()) . '">' . $menu_item->title . '</a></li>')
	@endif
@endforeach
@if ($item)
	<ul>{!! $item !!}</ul>
@endif


