@php ($menu_item_id = isset($options->menu_item_id)? $options->menu_item_id: '')
@foreach($items as $menu_item)
	@if (empty($menu_item->url))
		<option disabled>-- {{ $menu_item->title }} --</option>
		@if (count($menu_item->children))
			@foreach($menu_item->children as $menu_child)
				@php ($selected_child = $menu_item_id == $menu_child->id? ' selected': '')
				<option value="{{ $menu_child->id }}"{{ $selected_child }}>{{ $menu_child->title }}</option>
			@endforeach
		@endif
	@else
		@php ($selected_item = $menu_item_id == $menu_item->id? ' selected': '')
		<option value="{{ $menu_item->id }}"{{ $selected_item }}>{{ $menu_item->title }}</option>
	@endif
@endforeach