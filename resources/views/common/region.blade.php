<div id="select_region" class="select_region">
    <h3>Выбор города</h3>
    <div class="wrapp row_flex">
        <form id="region_form" action="{{ route('site.set.region') }}">
            {{ csrf_field() }}
            <input type="hidden" name="region" value="">
            <div class="col">
            @foreach ($top_regions as $top_region)
            <a data-region="{{ $top_region->id }}" href="#">{{ $top_region->type_short }}. {{ $top_region->name }}</a>
            @endforeach
            @php
                $let = '';
                $count = 1;
                $row = ceil(count($regions)/3);
            @endphp
            @foreach ($regions as $region)
                @php ($letter = mb_substr($region->name, 0, 1, 'UTF-8'))
                @if ($letter != $let)
                {!! $let ? '</ul>' : '' !!}
                    @if ($count > $row)
                        @php ($count = 1)
            </div>
            <div class="col">
                    @endif
                <strong>{{ $letter }}</strong>
                <ul>
                @php ($let = $letter)
                @endif
                    <li><a data-region="{{ $region->id }}" href="#">{{ $region->type_short }}. {{ $region->name }}</a></li>
                @php ($count++)
            @endforeach
                </ul>
                <a data-region="0" href="#">Все города</a>
            </div>
        </form>
    </div>
</div>