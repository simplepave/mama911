<div class="search">
    <section>
        <form id="search-form" action="{{ route('site.search') }}">
        	@php ($search = request()->get('q')?: '')
            <input value="{{ $search }}" name="search" class="input" type="text" placeholder="Поиск по услугам, специалистам">
            <input class="submit" type="submit" value="Найти">
        </form>
    </section>
</div>