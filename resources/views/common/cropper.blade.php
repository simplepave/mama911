<div id="modal-cropper" class="modal-cropper" style="position: fixed;top: 0;right: 0;bottom: 0;left: 0;background: rgba(0,0,0,0.8);z-index: 99999;display: none;">
    <div class="modal-dialog" style="position: relative;margin: 1% auto;background: #fff;padding: 5px 20px 13px 20px;">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Область для профильной фотографии</h4>
            </div>
            <div class="modal-body">
                <div class="image-container">
                    <img id="cropper-image" src="" />
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="cropper-close">Закрыть</button>
                <button type="button" class="crop-upload">Сохранить</button>
            </div>
        </div>
    </div>
</div>