<header>
    <a class="logo" href="{{ url('/') }}"></a>
    <div class="region-block-new">
        <a class="address popup" href="#select_region">{{ session('region_title') ?: 'Выбор города' }}</a>
        @inject('geoLocation', 'App\Http\Controllers\RegionController')
        @php ($geo = $geoLocation->geoLocation())
        @if ($geo['geo_region_title'])
        <div class="geo-block">
            Ваш город: {{ $geo['geo_region_title'] }}
            @if ($geo['geo_region'] and $geo['geo_region'] != session('region'))
            <form id="geoRegion-form" action="{{ route('site.set.region') }}">
                {{ csrf_field() }}
                <a data-region="{{ $geo['geo_region'] }}" href="#">Выбрать?</a>
                <input type="hidden" name="region" value="">
            </form>
            @endif
        </div>
        @endif
    </div>
    <div class="call_back">
        @guest
        <a class="popup" href="#entrance">Вход</a>
        <div><a class="popup" href="#registration">Регистрация для специалистов</a></div>
        @endguest
        @auth
        @if (Gate::allows('is.admin'))
        <a class="" href="{{ route('voyager.dashboard') }}" target="_blank">Админ панель</a>
        @endif
        @can('profile', App\User::class)
        <a class="" href="{{ route('site.profile') }}">Личный кабинет</a>
        @endcan
        <a href="{{ route('site.auth.logout') }}">Выход</a>
        @endauth
    </div>
</header>