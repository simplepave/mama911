<div class="bread-crumbs">
    <ul>
    	<li><a href="{{ url('/') }}">Главная</a></li>
    @foreach ($breadcrumbs as $breadcrumb)
        <li{!! $loop->last ? ' class="active"' : '' !!}>
            <a href="{{ $breadcrumb['href'] }}">{{ $breadcrumb['text'] }}</a>
        </li>
    @endforeach
    </ul>
</div>