<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('phone')->nullable();
            $table->integer('specialty')->unsigned()->index();
            $table->string('education')->nullable();
            $table->string('experience')->nullable();
            $table->integer('region_id')->unsigned()->index();
            $table->string('address')->nullable();
            $table->unsignedDecimal('price', 15, 2)->nullable();
            $table->unsignedDecimal('price_hour', 15, 2)->nullable();
            $table->text('about')->nullable();
            $table->string('photo_doc')->nullable();
            $table->string('photo_customer')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
