<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 *	Test
 */

Route::get('/test', 'TestController@index');
// Route::get('/test/geolocation/{ip}', 'TestController@geoLocation');

// Route::get('/mailable', function () {
//     $user = App\Models\User::find(44);
//     Illuminate\Support\Facades\Mail::to($user)->send(new App\Mail\ConfirmEmail($user));
//     // return new App\Mail\ConfirmEmail($user);
// });

// Route::get('/testmail', function () {
//     Mail::send('emails.test-email', ['name' => 'Mama911'], function ($message) {
//         $message->to('info@simplepave.ru', 'Джон Смит')->subject('Привет!');
//     });
// });

/**
 * Home
 */

Route::get('/', 'HomeController@index')
	->name('site.home');

/**
 *	Auth
 */

Route::post('/login', 'AuthController@loginPost')
	->name('site.auth.loginPost');

Route::post('/register', 'AuthController@registerPost')
	->name('site.auth.registerPost');

Route::get('/confirm/{token}', 'AuthController@confirmEmail')
	->name('site.auth.confirmEmail');

Route::get('/logout', 'AuthController@logout')
	->name('site.auth.logout');

/**
 *	Upload
 */

Route::post('/upload/save_doc', 'UploadController@saveDocPost')
	->middleware('profile')
	->name('site.upload.saveDocPost');

Route::post('/upload/save_cust', 'UploadController@saveCustPost')
	->middleware('profile')
	->name('site.upload.saveCustPost');

Route::post('/upload/remove_doc', 'UploadController@deleteDocPost')
	->middleware('profile')
	->name('site.upload.deleteDocPost');

Route::post('/upload/remove_cust', 'UploadController@deleteCustPost')
	->middleware('profile')
	->name('site.upload.deleteCustPost');

Route::post('/upload/remove_doc_cur/{id}', 'UploadController@deleteDocCurPost')
	->middleware('profile')
	->where('id', '[0-9]+')
	->name('site.upload.deleteDocCurPost');

Route::post('/upload/remove_cust_cur/{id}', 'UploadController@deleteCustCurPost')
	->middleware('profile')
	->where('id', '[0-9]+')
	->name('site.upload.deleteCustCurPost');

/**
 * Search
 */

Route::get('/search', 'SearchController@index')
	->name('site.search');

/**
 * Set Region
 */

Route::post('/region/set', 'RegionController@regionPost')
	->name('site.set.region');

/**
 * Profile
 * Card
 */

Route::get('/profile', 'CustomerController@profile')
	->middleware('profile')
	->name('site.profile');

Route::post('/profile', 'CustomerController@profilePost')
	->middleware('profile')
	->name('site.profilePost');

Route::get('/profile/{id}', 'CustomerController@userCard')
	->where('id', '[0-9]+')
	->name('site.user.card');

/**
 * Categories
 */

Route::get('/babies/{two?}/{three?}', 'CategoryController@babies')
	->name('site.babies');

Route::get('/future_mothers/{two?}/{three?}', 'CategoryController@future_mothers')
	->name('site.future_mothers');

Route::get('/moms/{two?}/{three?}', 'CategoryController@moms')
	->name('site.moms');

/**
 * Review
 */

Route::post('/review/add', 'ReviewController@addPost')
	->name('site.review.addPost');

Route::post('/review/next', 'ReviewController@next')
	->name('site.review.next');

/**
 * Articles
 */

Route::get('/articles', 'PostController@index')
	->name('site.articles');

Route::get('/article/{slug}', 'PostController@article')
	->name('site.article');

/**
 * Pages
 */

Route::get('/page/{slug}', 'PageController@page')
	->name('site.pages');

/**
 * Voyager
 */

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
