<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * Get the user
     */

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * Get the region
     */

    public function region()
    {
        return $this->belongsTo('App\Models\Region');
    }

    /**
     * Get the MenuItem
     */

    public function menu_item()
    {
        return $this->belongsTo('TCG\Voyager\Models\MenuItem', 'specialty');
    }

    /**
     * Get the review
     */

    public function review()
    {
        return $this->hasMany('App\Models\Review');
    }

    public function avgRating()
    {
        return $this->review()
          ->selectRaw('avg(rating) as aggregate')
          ->groupBy('customer_id');
    }

    /**
     * Get the specialty
     */

    public function specialty()
    {
        return $this->hasMany('App\Models\Specialty');
    }

    public function specialties()
    {
        return $this->specialty()
            ->selectRaw('GROUP_CONCAT(DISTINCT menu_items.title SEPARATOR \', \') as titles')
            ->join('menu_items', 'menu_items.id', '=', 'specialties.specialty');
    }
}
