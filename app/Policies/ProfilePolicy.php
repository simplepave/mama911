<?php

namespace App\Policies;

use App\Models\Customer;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class ProfilePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function profile()
    {
        if (Auth::check()) {
            $customer = Customer::where('user_id', Auth::id())->first();

            if ($customer)
                return 1 === 1;
        }

        return false;
    }
}
