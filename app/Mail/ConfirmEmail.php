<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ConfirmEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;

    /**
     * Create a new message instance.
     */

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     */

    public function build()
    {
        $token = $this->user->id . 'q' . md5($this->user->email . config('app.key'));
        $link = route('site.auth.confirmEmail', ['token' => $token]);

        return $this->markdown('emails.confirm-email')
                    ->with([
                        'url' => $link,
                        'user_name' => $this->user->name,
                    ]);
    }
}
