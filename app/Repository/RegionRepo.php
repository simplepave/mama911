<?php

namespace App\Repository;

use App\Models\Region;

class RegionRepo
{
	/**
     * Register Region
     */

    public function register_region($request)
    {
    	$region = Region::select('id')
    		->where('kladr', $request->input('region_kladr'))
    		->first();

    	if ($region)
    		return $region->id;

        $data = [
            'kladr' => $request->input('region_kladr'),
            'okato' => $request->input('region_okato'),
            'name' => $request->input('region'),
            'type' => $request->input('region_type'),
            'type_short' => $request->input('region_type_short'),
        ];

        $newRegionModel = Region::create($data);

        return $newRegionModel? $newRegionModel->id: false;
    }
}