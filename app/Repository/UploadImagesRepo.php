<?php

namespace App\Repository;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use App\Models\Upload;

class UploadImagesRepo
{
    private $photos_path;

    public function __construct()
    {
        $this->photos_path = storage_path('app/public') . DIRECTORY_SEPARATOR;
    }

    /**
     * Collection
     */

    public function collection($item)
    {
        $item = json_decode($item);

        if (is_object($item) && !empty($item->id))
            return Upload::whereIn('id', explode(',', $item->id))->get();

        return false;
    }

    /**
     * User Photo Upload
     */

    public function user_upload($user_id, $filename, $resized_name, $original_name)
    {
        if ($filename) {
            $newUploadModel = Upload::create([
                'user_id' => $user_id,
                'filename' => $filename,
                'resized_name' => $resized_name,
                'original_name' => $original_name,
            ]);

            if ($newUploadModel)
                return $newUploadModel->id;
        }

        return false;
    }

    /**
     * User Photo
     */

    public function user_photo($file, $width = null, $height = null, $path = 'default', $user_id = null)
    {
        if (!is_dir($this->photos_path . $path))
            File::makeDirectory($this->photos_path . $path, 0777, true);

        if (!$width and !$height) {
            $width = 250;
            $height = 250;
        }

        $obj_img = Image::make($file);
        $extension = $this->photo_mime($obj_img->mime());

        if (!$extension) return false;

        if ($width and $height)
            $img_resize = $obj_img->resize($width, $height);
        else
            $img_resize = $obj_img->resize($width, $height, function ($constraints) {
                $constraints->aspectRatio();
            });

        $name = sha1(date('YmdHis') . str_random(30));
        $img_size = $img_resize->getSize();
        $resize_name = $name . '_' . $img_size->width . 'x' . $img_size->height;

        $save_upload = $img_resize->save($this->photos_path . $path . DIRECTORY_SEPARATOR . $resize_name . $extension);

        if (isset($save_upload->basename)) {
            $filename = $save_upload->basename;

            if (!$user_id)
                return $filename;

            $resized_name = $path . DIRECTORY_SEPARATOR . $filename;
            $original_name = $this->original_file($file, $extension, $path);

            return $this->user_upload($user_id, $filename, $resized_name, $original_name);
        }

        return false;
    }

    protected function photo_mime($mime)
    {
        switch ($mime) {
            case 'image/jpeg': $extension = '.jpg'; break;
            case 'image/png': $extension = '.png'; break;
            case 'image/gif': $extension = '.gif'; break;
        }

        return isset($extension)? $extension: false;
    }

    /**
     * Saving images uploaded through XHR Request.
     */

    public function store($photos, $width = null, $height = null, $path = 'default')
    {
        if (!is_dir($this->photos_path . $path))
            File::makeDirectory($this->photos_path . $path, 0777, true);

        if (!$width and !$height) {
            $width = 250;
            $height = 250;
        }

        $data = [];

        for ($i = 0; $i < count($photos); $i++) {
            $photo = $photos[$i];
            $obj_img = Image::make($photo);

            if ($width and $height)
                $img_resize = $obj_img->resize($width, $height);
            else
                $img_resize = $obj_img->resize($width, $height, function ($constraints) {
                    $constraints->aspectRatio();
                });

            $name = sha1(date('YmdHis') . str_random(30));
            $img_size = $img_resize->getSize();
            $resize_name = $name . '_' . $img_size->width . 'x' . $img_size->height;
            $extension = '.' . $photo->getClientOriginalExtension();

            $save_upload = $img_resize->save($this->photos_path . $path . DIRECTORY_SEPARATOR . $resize_name . $extension);

            if (isset($save_upload->basename)) {
                $filename = $save_upload->basename;
                $resized_name = $path . DIRECTORY_SEPARATOR . $filename;
                $original_name = $this->original_file($photo, $extension, $path);

                $upload_id = $this->user_upload(auth()->user()->id, $filename, $resized_name, $original_name);

                if ($upload_id)
                    array_push($data, $upload_id);
            }
        }

        return $data?: false;
    }

    /**
     * Original File
     */

    private function original_file($photo, $extension, $path, $width = null, $height = null)
    {
    	$obj_img = Image::make($photo);

        if (!$width and !$height) {
            $width = null;
            $height = 500;
        }

        if ($width and $height)
            $img_resize = $obj_img->resize($width, $height);
        else
            $img_resize = $obj_img->resize($width, $height, function ($constraints) {
                $constraints->aspectRatio();
            });

        $name = sha1(date('YmdHis') . str_random(30));
        $img_size = $img_resize->getSize();
        $resize_name = $name . '_full_' . $img_size->width . 'x' . $img_size->height;

        $save_upload = $img_resize->save($this->photos_path . $path . DIRECTORY_SEPARATOR . $resize_name . $extension);

        if (isset($save_upload->basename))
            return $path . DIRECTORY_SEPARATOR . $save_upload->basename;

        return '';
    }

    /**
     * Remove Docs and Customers
     */

    public function destroy($upload_id)
    {
        $upload = Upload::find($upload_id);

        if ($upload) {
            $resized_file = $this->photos_path . $upload->resized_name;
            $original_name = $this->photos_path . $upload->original_name;

            if (file_exists($resized_file))
                unlink($resized_file);

            if ($upload->original_name && file_exists($original_name))
                unlink($original_name);

            $upload->delete();

            return true;
        }

        return false;
    }

    /**
     * Remove User Avatar
     */

    public function user_destroy($avatar)
    {
        $avatar = $this->photos_path . $avatar;

        if (file_exists($avatar)) {
            unlink($avatar);

            return true;
        }

        return false;
    }
}