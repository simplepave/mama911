<?php

if (!function_exists('menu_count_customer')) {
    function menu_count_customer($menu = true)
    {
        if ($menu) {
            $count = App\Models\Customer::join('users', 'users.id', '=', 'customers.user_id')
                ->join('specialties', 'specialties.customer_id', '=', 'customers.id')
                ->join('menu_items', 'menu_items.id', '=', 'specialties.specialty')
                ->join('menus', 'menus.id', '=', 'menu_items.menu_id')
                ->join('regions', 'regions.id', '=', 'customers.region_id')
                ->where('users.status', 1)
                ->where('menus.name', $menu);

            if (session()->has('region'))
                $count->where('regions.id', session('region'));

            return $count->count();
        }
    }
}
