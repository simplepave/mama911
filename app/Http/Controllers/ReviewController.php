<?php

namespace App\Http\Controllers;

use App\Models\Review;
use Illuminate\Support\Facades\DB;

class ReviewController extends Controller
{
    private $paginate = 3;

    /**
     *
     */

    public function index()
    {
        //
    }

    /**
     * Add Review
     */

    public function addPost()
    {
        \Debugbar::disable();

        $this->validate($this->request, [
            'author' => 'required|max:255',
            'email' => 'required|regex:/.+@.+\..+/i|max:255',
            'text' => 'required|max:3000|min:15',
        ]);

        $data = [
            'customer_id' => $this->request->input('customer_id'),
            'author' => $this->request->input('author'),
            'email' => $this->request->input('email'),
            'rating' => $this->request->input('rating'),
            'text' => $this->request->input('text'),
        ];

        $newReviewModel = Review::create($data);

        if ($newReviewModel)
            return response()->json([
                'success' => 'Благодарим Вас за отзыв.',
                'author' => $newReviewModel->author,
                'text' => $newReviewModel->text,
                'created_at' => \Carbon\Carbon::parse($newReviewModel->created_at)->format('d.m.Y'),
            ]);

        return response()->json(['error' => 'Ошибка, что-то пошло не так!']);
    }

    /**
     * Next
     */

    public function next()
    {
        \Debugbar::disable();

        if ($this->request->input('customer')) {
            $reviews = Review::select('author', 'text', DB::raw('DATE_FORMAT(`created_at`, "%d.%m.%Y") as `date`'))
                        ->where('status', 1)
                        ->where('customer_id', $this->request->input('customer'))
                        ->orderBy('id', 'desc')
                        ->paginate($this->paginate);

            return response()->json([
                'success' => true,
                'reviews' => $reviews,
                'more' => $reviews->hasMorePages(),
                'next' => route('site.review.next') . '?page=' . ($reviews->currentPage() + 1),
            ]);
        }

        return response()->json(['error' => 'Ошибка, что-то пошло не так!']);
    }
}
