<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Models\Region;
use App\Models\Customer;
use App\Classes\GeoIp;

class RegionController extends Controller
{
    /**
     * Regions
     */

    public function index()
    {
        $customers = Customer::select(DB::raw("GROUP_CONCAT(DISTINCT customers.region_id SEPARATOR ',')  AS region_id"))
            ->join('users', 'users.id', '=', 'customers.user_id')
            ->where('users.status', 1)
            ->where('customers.region_id', '<>', 0)
            ->first();

        $region_id = explode(',', $customers->region_id);
        $top_arr = ['7700000000000', '5000000000000', '7800000000000', '4700000000000'];

        $regions = Region::select('id', 'name', 'type_short')
            ->whereIn('id', $region_id)
            ->whereNotIn('kladr', $top_arr)
            ->orderBy('name')
            ->get();

        $top_regions = Region::select('id', 'name', 'type_short')
            ->whereIn('id', $region_id)
            ->whereIn('kladr', $top_arr)
            ->orderBy('type_short')
            ->get();

        return view('common.region', [
            'regions' => $regions,
            'top_regions' => $top_regions,
        ]);
    }

    /**
     * Region Post Set Session
     */

    public function regionPost()
    {
        $region_id = $this->request->input('region');

        if (isset($region_id)) {

            if ($region_id === '0') {
                $this->request->session()->forget('region');
                $this->request->session()->forget('region_title');
                $this->request->session()->forget('geo_region');
                $this->request->session()->forget('geo_region_title');

                return response()
                    ->json(['success' => true]);
            }

            $region = Region::select('id', DB::raw('CONCAT(type_short, \'. \', name)  AS title'))
                ->find($region_id);

            if ($region) {
                session([
                    'region' => $region->id,
                    'region_title' => $region->title,
                ]);

                return response()
                    ->json(['success' => true]);
            }
        }
    }

    /**
     * Geo Location
     */

    public function geoLocation()
    {
        $this->ip();

        if (!session()->has('geo_region') || !session()->has('geo_region_title'))
            $region = GeoIp::getCity();

        if (!session()->has('geo_region')) {
            if (isset($region['kladr'])) {
                $region_id = Region::select('regions.id')
                    ->join('customers', 'customers.region_id', '=', 'regions.id')
                    ->join('users', 'users.id', '=', 'customers.user_id')
                    ->where('users.status', 1)
                    ->where('regions.kladr', $region['kladr'])
                    ->first();

                if ($region_id)
                    session(['geo_region' => $region_id->id]);
                else
                    session(['geo_region' => false]);
            }
            else
                session(['geo_region' => false]);
        }

        if (!session()->has('geo_region_title')) {
            if (isset($region['full_name']))
                session(['geo_region_title' => $region['full_name']]);
            else
                session(['geo_region_title' => false]);
        }

        $data['geo_region'] = session('geo_region');
        $data['geo_region_title'] = session('geo_region_title');

        return $data;
    }

    /**
     * IP Location
     */

    public function ip()
    {
        $ip = request()->ip()?: false;

        if ($ip) {
            if (!session()->has('ip'))
                session(['ip' => $ip]);
            elseif (session('ip') != $ip) {
                session(['ip' => $ip]);
                $this->request->session()->forget('geo_region');
                $this->request->session()->forget('geo_region_title');
            }
        }
    }
}
