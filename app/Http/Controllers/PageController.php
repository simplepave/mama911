<?php

namespace App\Http\Controllers;

use App\Models\Page;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     */

    public function page($slug)
    {
        $page = Page::select('id', 'title', 'excerpt', 'body', 'image', 'slug', 'meta_keywords')
            ->where('status', 'ACTIVE')
            ->where('slug', $slug)
            ->first();

        if ($page) {
            $title = $page->title;

            $breadcrumbs[] = [
                'text' => $title,
                'href' => route('site.pages', ['slug' => $slug]),
            ];

            return view('pages.page', [
                'title' => $title,
                'keywords' => $page->meta_keywords,
                'page' => $page,
                'breadcrumbs' => $breadcrumbs,
            ]);
        }

        abort(404);
    }
}
