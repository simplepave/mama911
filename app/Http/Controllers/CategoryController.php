<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use TCG\Voyager\Models\MenuItem;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    private $paginate = 7;
    private $url = false;

    /**
     * Babies
     */

    public function babies($two = false, $three = false)
    {
        $title = 'Малышам';

        $breadcrumbs[] = [
            'text' => $title,
            'href' => route('site.babies'),
        ];

        if ($two || $three) {
            $this->url = '/' . $this->request->path();

            $breadcrumbs[] = [
                'text' => $this->menu_items(),
                'href' => url($this->url),
            ];
        }

        return view('pages.babies', [
            'title' => $title,
            'customers' => $this->customers(),
            'breadcrumbs' => $breadcrumbs,
        ]);
    }

    /**
     * Moms
     */

    public function moms($two = false, $three = false)
    {
        $title = 'Мамам';

        $breadcrumbs[] = [
            'text' => $title,
            'href' => route('site.moms'),
        ];

        if ($two || $three) {
            $this->url = '/' . $this->request->path();

            $breadcrumbs[] = [
                'text' => $this->menu_items(),
                'href' => url($this->url),
            ];
        }

        return view('pages.moms', [
            'title' => $title,
            'customers' => $this->customers(),
            'breadcrumbs' => $breadcrumbs,
        ]);
    }

    /**
     * Future Mothers
     */

    public function future_mothers($two = false, $three = false)
    {
        $title = 'Будущим мамам';

        $breadcrumbs[] = [
            'text' => $title,
            'href' => route('site.future_mothers'),
        ];

        if ($two || $three) {
            $this->url = '/' . $this->request->path();

            $breadcrumbs[] = [
                'text' => $this->menu_items(),
                'href' => url($this->url),
            ];
        }

        return view('pages.future_mothers', [
            'title' => $title,
            'customers' => $this->customers(),
            'breadcrumbs' => $breadcrumbs,
        ]);
    }

    /**
     * Customers
     */

    private function customers()
    {
        $menu = explode('/', $this->request->path(), 2);

        $customers = Customer::select('users.id', 'users.name', 'users.avatar','customers.price', 'customers.price_hour', 'regions.name as region_name', 'regions.type_short as region_type', DB::raw('(SELECT AVG(reviews.rating) AS total FROM reviews WHERE reviews.customer_id = customers.id AND reviews.status = 1 GROUP BY reviews.customer_id) as aggregate'), DB::raw('(SELECT GROUP_CONCAT(DISTINCT m.title SEPARATOR ", ") AS titles FROM `specialties` as s INNER JOIN `menu_items` as m ON m.id = s.specialty WHERE s.customer_id = customers.id) as `specialty`'))
            ->join('users', 'users.id', '=', 'customers.user_id')
            ->join('specialties', 'specialties.customer_id', '=', 'customers.id')
            ->join('regions', 'regions.id', '=', 'customers.region_id')
            ->join('menu_items', 'menu_items.id', '=', 'specialties.specialty');

        if ($this->url)
            $customers->where('menu_items.url', $this->url);
        else
            $customers->join('menus', 'menus.id', '=', 'menu_items.menu_id')
                ->where('menus.name', $menu[0]);

        if (session()->has('region'))
            $customers->where('regions.id', session('region'));

        return $customers->where('users.status', 1)
            ->orderBy('customers.id', 'desc')
            ->groupBy('customers.id')
            ->paginate($this->paginate);
    }

    /**
     * Menu Items
     */

    private function menu_items()
    {
        $title = MenuItem::where('url', $this->url)->first();

        if ($title)
            return $title->title;

        abort(404);
    }
}
