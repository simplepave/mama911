<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Repository\UploadImagesRepo;

class UploadController extends Controller
{
    /**
     *
     */

    public function index()
    {
        //
    }

    /**
     * Save Image Doc
     */

    public function saveDocPost(UploadImagesRepo $upload)
    {
        $photos = $this->request->file('file');

        if (!is_array($photos))
            $photos = [$photos];

        $path = 'documents' . DIRECTORY_SEPARATOR . date('FY');
        $upload_ids = $upload->store($photos, null, 127, $path);

        if ($upload_ids) {
            $ids = $upload_ids;
            $customer = Customer::where('user_id', auth()->user()->id)->first();

            if ($customer->photo_doc) {
                $json = json_decode($customer->photo_doc);
                if (isset($json->id)) {
                    $arr = explode(',', $json->id);
                    $upload_ids = array_merge($arr, $upload_ids);
                }
            }

            $upload_ids = implode(',', $upload_ids);
            $customer->photo_doc = json_encode(['id' => (string)$upload_ids]);
            $customer->save();

            return response()->json([
                    'message' => 'Image saved Successfully',
                    'id' => implode(',', $ids),
                ], 200);
        }

        return response()->json([
                'message' => 'Error',
            ], 200);
    }

    /**
     * Save Image Customers
     */

    public function saveCustPost(UploadImagesRepo $upload)
    {
        $photos = $this->request->file('file');

        if (!is_array($photos))
            $photos = [$photos];

        $path = 'customer' . DIRECTORY_SEPARATOR . date('FY');
        $upload_ids = $upload->store($photos, null, 127, $path);

        if ($upload_ids) {
            $ids = $upload_ids;
            $customer = Customer::where('user_id', auth()->user()->id)->first();

            if ($customer->photo_customer) {
                $json = json_decode($customer->photo_customer);
                if (isset($json->id)) {
                    $arr = explode(',', $json->id);
                    $upload_ids = array_merge($arr, $upload_ids);
                }
            }

            $upload_ids = implode(',', $upload_ids);
            $customer->photo_customer = json_encode(['id' => (string)$upload_ids]);
            $customer->save();

            return response()->json([
                    'message' => 'Image saved Successfully',
                    'id' => implode(',', $ids),
                ], 200);
        }

        return response()->json([
                'message' => 'Error',
            ], 200);
    }

    /**
     * Delete Image Doc
     */

    public function deleteDocPost(UploadImagesRepo $upload)
    {
        $data = $this->request->data;
        $uuid = $this->request->uuid;

        if ($data && $uuid) {
            $data = json_decode($data, true);

            foreach ($data as $value) {
                if ($value['uuid'] == $uuid) {
                    $destroy = $upload->destroy($value['uploadId']);

                    if ($destroy) {
                        $customer = Customer::where('user_id', auth()->user()->id)->first();

                        if ($customer->photo_doc) {
                            $json = json_decode($customer->photo_doc);
                            if (isset($json->id)) {
                                $arr = explode(',', $json->id);
                                $key = array_search($value['uploadId'], $arr);

                                if ($key !== false)
                                    unset($arr[$key]);

                                $upload_ids = implode(',', $arr);
                            }
                        }

                        $customer->photo_doc = json_encode(['id' => (string)$upload_ids]);
                        $customer->save();
                    }
                }
            }

            return response()->json([
                    'message' => 'File successfully delete',
                ], 200);
        }

        return response()->json([
                'message' => 'Error',
            ], 200);
    }

    /**
     * Delete Image Customers
     */

    public function deleteCustPost(UploadImagesRepo $upload)
    {
        $data = $this->request->data;
        $uuid = $this->request->uuid;

        if ($data && $uuid) {
            $data = json_decode($data, true);

            foreach ($data as $value) {
                if ($value['uuid'] == $uuid) {
                    $destroy = $upload->destroy($value['uploadId']);

                    if ($destroy) {
                        $customer = Customer::where('user_id', auth()->user()->id)->first();

                        if ($customer->photo_customer) {
                            $json = json_decode($customer->photo_customer);
                            if (isset($json->id)) {
                                $arr = explode(',', $json->id);
                                $key = array_search($value['uploadId'], $arr);

                                if ($key !== false)
                                    unset($arr[$key]);

                                $upload_ids = implode(',', $arr);
                            }
                        }

                        $customer->photo_customer = json_encode(['id' => (string)$upload_ids]);
                        $customer->save();
                    }
                }
            }

            return response()->json([
                    'message' => 'File successfully delete',
                ], 200);
        }

        return response()->json([
                'message' => 'Error',
            ], 200);
    }

    /**
     * Delete Image Doc Curr
     */

    public function deleteDocCurPost(UploadImagesRepo $upload, $id)
    {
        $destroy = $upload->destroy($id);

        if ($destroy) {
            $customer = Customer::where('user_id', auth()->user()->id)->first();

            if ($customer->photo_doc) {
                $json = json_decode($customer->photo_doc);
                if (isset($json->id)) {
                    $arr = explode(',', $json->id);
                    $key = array_search($id, $arr);

                    if ($key !== false)
                        unset($arr[$key]);

                    $upload_ids = implode(',', $arr);
                }
            }

            $customer->photo_doc = json_encode(['id' => (string)$upload_ids]);
            $customer->save();

            return response()->json([
                    'message' => 'File successfully delete',
                ], 200);
        }

        return response()->json([
                'message' => 'Error',
            ], 200);
    }

    /**
     * Delete Image Customers Curr
     */

    public function deleteCustCurPost(UploadImagesRepo $upload, $id)
    {
        $destroy = $upload->destroy($id);

        if ($destroy) {
            $customer = Customer::where('user_id', auth()->user()->id)->first();

            if ($customer->photo_customer) {
                $json = json_decode($customer->photo_customer);
                if (isset($json->id)) {
                    $arr = explode(',', $json->id);
                    $key = array_search($id, $arr);

                    if ($key !== false)
                        unset($arr[$key]);

                    $upload_ids = implode(',', $arr);
                }
            }

            $customer->photo_customer = json_encode(['id' => (string)$upload_ids]);
            $customer->save();

            return response()->json([
                    'message' => 'File successfully delete',
                ], 200);
        }

        return response()->json([
                'message' => 'Error',
            ], 200);
    }
}
