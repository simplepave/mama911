<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Category;

class PostController extends Controller
{
    private $paginate = 9;
    private $category_id = 1;

    /**
     *
     */

    public function index()
    {
        $posts = Post::select('title', 'image', 'slug')
            ->where('status', 'PUBLISHED')
            ->where('category_id', $this->category_id)
            ->orderBy('id', 'desc')
            ->paginate($this->paginate);

        $title = Category::select('name')
            ->findOrFail($this->category_id)
            ->name;

        $breadcrumbs[] = [
            'text' => $title,
            'href' => route('site.articles'),
        ];

        return view('pages.articles', [
            'title' => $title,
            'posts' => $posts,
            'breadcrumbs' => $breadcrumbs,
        ]);
    }

    /**
     *
     */

    public function article($slug)
    {
        $post = Post::select('id', 'category_id', 'title', 'excerpt', 'body', 'image', 'slug', 'meta_keywords')
            ->where('status', 'PUBLISHED')
            ->where('category_id', $this->category_id)
            ->where('slug', $slug)
            ->first();

        if ($post) {
            $title = $post->title;

            $breadcrumbs[] = [
                'text' => $post->category->name,
                'href' => route('site.articles'),
            ];

            $breadcrumbs[] = [
                'text' => $title,
                'href' => route('site.article', ['slug' => $slug]),
            ];

            $other_articles = $this->articles_random($post->id);

            return view('pages.article', [
                'title' => $title,
                'keywords' => $post->meta_keywords,
                'post' => $post,
                'other_articles' => $other_articles,
                'breadcrumbs' => $breadcrumbs,
            ]);
        }

        abort(404);
    }

    /**
     *
     */

    public function articles_random($id)
    {
        $posts = Post::select('title', 'image', 'slug')
            ->where('status', 'PUBLISHED')
            ->where('category_id', $this->category_id)
            ->where('id', '<>', $id)
            ->inRandomOrder()
            ->orderBy('id', 'desc')
            ->limit(3)
            ->get();

        return $posts;
    }

    /**
     *
     */

    public function articles_home()
    {
        $posts = Post::select('title', 'image', 'slug')
            ->where('status', 'PUBLISHED')
            ->where('category_id', $this->category_id)
            ->orderBy('id', 'desc')
            ->limit(3)
            ->get();

        return $posts;
    }
}
