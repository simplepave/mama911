<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Http\Controllers\PostController;

class HomeController extends Controller
{
    /**
     *
     */

    public function index(PostController $post)
    {
		$customers = Customer::select('users.id', 'users.avatar', 'menu_items.title')
            ->join('users', 'users.id', '=', 'customers.user_id')
            ->join('menu_items', 'menu_items.id', '=', 'customers.specialty')
            ->where('users.status', 1)
            ->orderBy('id', 'desc')
            ->limit(10)
            ->get();

        return view('pages.home', [
            'articles_home' => $post->articles_home(),
            'customers' => $customers,
        ]);
    }
}
