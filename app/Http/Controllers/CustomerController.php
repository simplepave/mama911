<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Customer;
use App\Models\Specialty;
use Illuminate\Support\Facades\Auth;
use App\Repository\UploadImagesRepo;
use App\Repository\RegionRepo;

class CustomerController extends Controller
{
    private $paginate = 3;

    /**
     * Display a listing of the resource.
     */

    public function index()
    {
        //
    }

    /**
     * Profile
     */

    public function profile(UploadImagesRepo $upload)
    {
        $user = User::where('status', 1)->find(Auth::id());

        if ($user) {
            $customer = $user->customer;
            $specialties = $customer->specialty()
                            ->where('specialty', '<>', '0')
                            ->orderBy('item')
                            ->get();
            $specialty = false;
            $photo_doc = $upload->collection($customer->photo_doc);
            $photo_customer = $upload->collection($customer->photo_customer);

            if ($specialties && count($specialties) < 5)
                $specialty = view('parts.specialty-select');

            return view('pages.profile', [
                'title' => 'Личный кабинет',
                'user' => $user,
                'customer' => $customer,
                'specialties' => $specialties,
                'specialty' => $specialty,
                'photo_doc' => $photo_doc,
                'photo_customer' => $photo_customer,
            ]);
        }

        abort(404);
    }

    /**
     * Profile Post
     */

    public function profilePost(UploadImagesRepo $upload)
    {
        $this->validate($this->request, [
            'name' => 'required|max:255',
            'phone' => 'required|regex:/^([+]?[0-9\s-\(\)]{3,25})*$/i|max:255|min:6',
            'specialty_1' => 'required',
            'education' => 'required|max:255|min:2',
            'experience' => 'required|max:255|min:1',
            'region' => 'required|max:255|min:2',
            'address' => 'required|max:255|min:2',
            'price' => 'required|numeric',
            'price_hour' => 'required|numeric',
            'about' => 'max:3000',
        ]);

        $user = User::where('status', 1)->find(Auth::id());

        if ($user) {

            if ($this->request->input('photo_customer')) {
                $file = $this->request->input('photo_customer');
                $path = 'users' . DIRECTORY_SEPARATOR . date('FY');
                $user_avatar = $upload->user_photo($file, 213, 213, $path);

                if ($user_avatar) {
                    if ($user->avatar && $user->avatar != 'users/default.png')
                        $upload->user_destroy($user->avatar);
                    $user->avatar = $path . DIRECTORY_SEPARATOR . $user_avatar;
                }
            }

            $user->name = $this->request->input('name');

            $saveUser = $user->save();
            $saveCustomer = $saveUser? $this->saveCustomer($user->id): false;

            if ($saveCustomer)
                return response()
                    ->json(['success' => 'Данные успешно обновлены!']);
        }

        return response()
            ->json(['error' => 'Ошибка, что-то пошло не так!']);
    }

    private function saveCustomer($user_id)
    {
        $customer = Customer::where('user_id', $user_id)->first();
        $this->saveSpecialty($customer);

        $customer->phone = $this->request->input('phone');
        // $customer->specialty = $this->request->specialty;
        $customer->education = $this->request->input('education');
        $customer->experience = $this->request->input('experience');
        $customer->address = $this->request->input('address');
        $customer->price = $this->request->input('price');
        $customer->price_hour = $this->request->input('price_hour');
        $customer->about = $this->request->input('about');

        if ($this->request->input('region_kladr')) {
            $region_id = (new RegionRepo)->register_region($this->request);

            if ($region_id)
                $customer->region_id = $region_id;
        }

        return $customer->save();
    }

    private function saveSpecialty($customer)
    {
        for ($i = 1; $i <= 5; $i++) {
            $item = $this->request->input('specialty_' . $i)?: 0;

            $specialty = Specialty::where('customer_id', $customer->id)
                            ->where('item', $i)
                            ->first();

            if ($specialty) {
                $specialty->specialty = $item;
                $specialty->save();
            }
            elseif ($item)
                $customer->specialty()->create([
                    'specialty' => $item,
                    'item' => $i,
                ]);
        }
    }

    /**
     * User Card
     */

    public function userCard(UploadImagesRepo $upload, $id)
    {
        $user = User::where('status', 1)->find($id);

        if ($user) {
            $customer = $user->customer;
            $photo_doc = $upload->collection($customer->photo_doc);
            $photo_customer = $upload->collection($customer->photo_customer);

            $reviews = $customer->review()
                        ->where('status', 1)
                        ->orderBy('id', 'desc')
                        ->paginate($this->paginate);

            return view('pages.user-card', [
                'title' => 'Карточка пользователя',
                'user' => $user,
                'customer' => $customer,
                'reviews' => $reviews,
                'photo_doc' => $photo_doc,
                'photo_customer' => $photo_customer,
            ]);
        }

        abort(404);
    }
}
