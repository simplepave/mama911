<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Mail\ConfirmEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use App\Repository\UploadImagesRepo;
use App\Repository\RegionRepo;


class AuthController extends Controller
{
    private $path_date;

    /**
     * Confirm Email
     */

    public function confirmEmail($token)
    {
        $token = explode('q', $token, 2);
        $user = User::findOrFail($token[0]);

        if (is_null($user->remember_token) && !empty($token[1])) {
            if (md5($user->email . config('app.key')) == $token[1]) {
                $user->status = 1;
                $user->save();

                Session::flash('confirmEmail', 'Вы успешно подтвердили E-mail!');
                Auth::login($user, true);
            }
        }

        return redirect()->route('site.home');
    }

    /**
     * Register
     */

    public function registerPost(UploadImagesRepo $upload)
    {
        \Debugbar::disable();

        $this->path_date = DIRECTORY_SEPARATOR . date('FY');

        $this->validate($this->request, [
            'name' => 'required|max:255',
            'email' => 'required|regex:/.+@.+\..+/i|unique:users|max:255',
            'password' => 'required|max:255|min:6',
            'password2' => 'required|same:password',
            'phone' => 'required|regex:/^([+]?[0-9\s-\(\)]{3,25})*$/i|max:255|min:6',
            'specialty' => 'required',
            // 'education' => 'required|max:255|min:2',
            // 'experience' => 'required|max:255|min:1',
            'region' => 'required|max:255|min:2',
            // 'address' => 'required|max:255|min:2',
            // 'price' => 'required|numeric',
            // 'price_hour' => 'required|numeric',
        ]);

        $data = [
            'role_id' => 2,
            'name' => $this->request->input('name'),
            'email' => $this->request->input('email'),
            'password' => bcrypt($this->request->input('password')),
        ];

        if ($this->request->input('photo_user_register')) {
            $file = $this->request->input('photo_user_register');
            $path = 'users' . $this->path_date;
            $photo_user_register = $upload->user_photo($file, 213, 213, $path);

            if ($photo_user_register)
                $data['avatar'] = $path . DIRECTORY_SEPARATOR . $photo_user_register;
        }

        $newUserModel = User::create($data);
        $newCustomerModel = $newUserModel? $this->registerCustomer($newUserModel): false;

        if ($newCustomerModel) {
            Mail::to($newUserModel)->send(new ConfirmEmail($newUserModel));

            return response()
                ->json(['auth_success' => trans('auth.auth_success')]);
        }

        return response()->json(['auth_error' => trans('auth.auth_error')]);
    }

    private function registerCustomer($user)
    {
        $upload = new UploadImagesRepo;

        $data = [
            'phone' => $this->request->input('phone'),
            'specialty' => $this->request->specialty,
            'education' => $this->request->input('education'),
            'experience' => $this->request->input('experience'),
            'address' => $this->request->input('address'),
            'price' => $this->request->input('price'),
            'price_hour' => $this->request->input('price_hour'),
        ];

        $region_id = (new RegionRepo)->register_region($this->request);
        $data['region_id'] = $region_id?: 0;

        if ($this->request->input('photo_user_doc')) {
            $file = $this->request->input('photo_user_doc');
            $path = 'documents' . $this->path_date;
            $lastInsertId = $upload->user_photo($file, null, 127, $path, $user->id);

            if ($lastInsertId)
                $data['photo_doc'] = json_encode(['id' => (string)$lastInsertId]);
        }

        if ($this->request->input('photo_user_customer')) {
            $file = $this->request->input('photo_user_customer');
            $path = 'customer' . $this->path_date;
            $lastInsertId = $upload->user_photo($file, null, 127, $path, $user->id);

            if ($lastInsertId)
                $data['photo_customer'] = json_encode(['id' => (string)$lastInsertId]);
        }

        $customer = $user->customer()->create($data);

        if ($customer) {
            $data = [
                'specialty' => $this->request->specialty,
                'item' => 1,
            ];

            $specialty = $customer->specialty()->create($data);

            if ($specialty)
                return true;
        }

        return false;
    }

    /**
     * Login
     */

    public function loginPost()
    {
        \Debugbar::disable();

        $user = User::where('email', $this->request->input('email'))->first();

        if ($user && !$user->status)
            return response()
                ->json(['auth_error' => trans('auth.access_denied')]);

        $auth_result = Auth::attempt([
            'email' => $this->request->input('email'),
            'password' => $this->request->input('password'),
            'status' => 1,
        ]);

        if ($auth_result) {
            if (Gate::allows('is.admin'))
                return response()
                    ->json(['auth_admin' => route('voyager.dashboard')]);

            elseif (Gate::allows('is.status'))
                return response()
                    ->json(['auth_back' => 1]);
        }

        return response()
            ->json(['auth_error' => trans('auth.wrong_password')]);
    }

    /**
     * Logout
     */

    public function logout()
    {
        Auth::logout();

        return redirect()->route('site.home');
    }
}
