<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    private $paginate = 7;

    /**
     * Search
     */

    public function index()
    {
        $title = 'Поиск';
        $search = $this->request->q? '?q=' . $this->request->q: '';

        $breadcrumbs[] = [
            'text' => $title,
            'href' => route('site.search') . $search,
        ];

        return view('pages.search', [
            'title' => $title,
            'customers' => $this->customers(),
            'breadcrumbs' => $breadcrumbs,
        ]);
    }

    /**
     * Customers
     */

    private function customers()
    {
        $search = $this->request->q;

        if (!$search) return false;

        $customers = Customer::select('users.id', 'users.name', 'users.avatar','customers.price', 'customers.price_hour', 'regions.name as region_name', 'regions.type_short as region_type', DB::raw('(SELECT AVG(reviews.rating) AS total FROM reviews WHERE reviews.customer_id = customers.id AND reviews.status = 1 GROUP BY reviews.customer_id) as aggregate'), DB::raw('(SELECT GROUP_CONCAT(DISTINCT m.title SEPARATOR ", ") AS titles FROM `specialties` as s INNER JOIN `menu_items` as m ON m.id = s.specialty WHERE s.customer_id = customers.id) as `specialty`'))
            ->join('users', 'users.id', '=', 'customers.user_id')
            ->join('specialties', 'specialties.customer_id', '=', 'customers.id')
            ->join('regions', 'regions.id', '=', 'customers.region_id')
            ->join('menu_items', 'menu_items.id', '=', 'specialties.specialty')
            ->where('users.status', 1)
            ->where('users.name', 'LIKE', '%' . $search . '%')
            ->orWhere('menu_items.title', 'LIKE', '%' . $search . '%');
            // ->whereRaw('LOWER(`users`.`name`) LIKE \'%' . $search . '%\'');

        if (session()->has('region'))
            $customers->where('regions.id', session('region'));

        return $customers->orderBy('customers.id', 'desc')->groupBy('customers.id')->paginate($this->paginate);
    }
}
