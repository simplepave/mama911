<?php

namespace App\Classes;

final class GeoIp
{
    private static $apiKey = '9a659f85070cebe5a91d04ae8df33cedcde02011';
    private static $dadata = 'http://suggestions.dadata.ru/suggestions/api/4_1/rs/detectAddressByIp?ip=';

    private static function detect($ip = false)
    {
        $ip = $ip?: request()->ip();

        if ($ip && $ch = curl_init(self::$dadata . $ip))
        {
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Accept: application/json',
                'Authorization: Token ' . self::$apiKey
            ));
            $result = curl_exec($ch);
            $result = json_decode($result);
            curl_close($ch);
        }

        return isset($result)? $result: false;
    }

    private static function data($ip = false)
    {
        $result = self::detect($ip);
        return $result && $result->location? $result->location->data: false;
    }

    private static function isdata($data, $item)
    {
        return isset($data->$item)? $data->$item: '';
    }

    /**
     * Array
     */

    public static function getArray($ip = false)
    {
        if ($data = self::data($ip))
            return json_decode(json_encode($data), true);

        return false;
    }

    /**
     * City
     */

    public static function getCity($ip = false)
    {
        if ($data = self::data($ip))
            return [
                'kladr' => self::isdata($data, 'city_kladr_id'),
                'full_name' => self::isdata($data, 'city_type') . '. ' . self::isdata($data, 'city'),
            ];

        return false;
    }

    /**
     * Region
     */

    public static function getRegion($ip = false)
    {
        if ($data = self::data($ip))
            return [
                'kladr' => self::isdata($data, 'region_kladr_id'),
                'full_name' => self::isdata($data, 'region_type') . '. ' . self::isdata($data, 'region'),
            ];

        return false;
    }
}